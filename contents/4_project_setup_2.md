## TA1 Primitive Development - 2: Pipeline

After building any primitives, it has to put into a pipeline and validated in
order to integrate into the `D3M` primitives:

**Note:** All the explainations below will be based on the [d3m_test](https://github.com/tonyjo/d3m_test) setup.

The esstential elements in building pipelines is:
```
 Dataset loader -> Dataset Parser -> Data Cleaner (In necessary) -> Classifier/Regression -> Output
```
#### 1. Import all the necessary packages

Packages can be directly imported from `d3m` or common primitives.
Import the necessary packages and the custom primitive.

```python
from d3m import index
from d3m.metadata import base as metadata_base
from d3m.metadata.base import Context, ArgumentType
from d3m.metadata.pipeline import Pipeline, PrimitiveStep

import d3m.primitives.data_cleaning.imputer as Imputer
import d3m.primitives.classification.random_forest as RF

# Testing primitive
from samplePrimitives.samplePrimitive1.input_to_output import InputToOutput

```

#### 2. Primitive step construction

Define primitive for each stage/step of the pipeline along with:

-`Inputs`

-`outputs`

-`hyperparameters`



**Sample Setup**

```python

step_6 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.classification.decision_tree.SKlearn'))
step_6.add_argument(name='inputs',  argument_type=ArgumentType.CONTAINER,  data_reference='steps.5.produce')
step_6.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets)
step_6.add_output('produce')
pipeline.add_step(step_6)
```

#### 3. Run and Validate `pipeline`

Once pipeline is constructed, `json` file is generated and using  `python3 -m d3m runtime`
to validate the pipeline.

```shell

sudo docker run --rm\
                -v ../local/path/d3m_test:/d3m_test\
                -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 /bin/bash\
                -c "cd /d3m_test;\
                    pip3 install -e .;\
                    cd pipelines;\
                    python3 primitive1_pipeline.py;\
                    python3 -m d3m runtime fit-produce \
                            -p pipeline.json \
                            -r /d3m_test/datasets/seed_datasets_current/38_sick/TRAIN/problem_TRAIN/problemDoc.json \
                            -i /d3m_test/datasets/seed_datasets_current/38_sick/TRAIN/dataset_TRAIN/datasetDoc.json \
                            -t /d3m_test/datasets/seed_datasets_current/38_sick/TEST/dataset_TEST/datasetDoc.json \
                            -o 38_sick_results.csv \
                            -O pipeline_run.yml;\
                    exit"

```
