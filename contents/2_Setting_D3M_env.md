## Setting up D3M environment

In order to run a pipeline, you must have a Python environment where the d3m
core package is installed, as well as the packages of the primitives installed
as well. While it is possible to setup a Python virtual environment and install
the packages them through `pip`, in this tutorial, we're going to use the D3M
docker images instead (in many cases, even beyond this tutorial, this will save
you a lot of time and effort trying to find the any missing primitive packages,
manually installing them, and troubleshooting installation errors). So, make
sure [Docker](https://docs.docker.com/) is installed in your system.

You can find the list of D3M docker images in the Public Docker images link
above. The one we're going to use in this tutorial is the v2020.1.9 primitives
image (feel free to use whatever the latest one instead though - just modify the
`v2020.1.9` part accordingly):

```shell
docker pull registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9
```

Once you have downloaded the image, make sure you can run the container:

```shell
docker run --rm -i -t registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 /bin/bash
```

Now we can finally run the d3m modules (and hence run a pipeline).
Before running a pipeline though, let's first try to get
a list of what primitives are installed in the image's Python environment:

```shell
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m index search
```

You should get a big list of primitives. All of the known primitives to D3M
should be there.

Note that you can access the help string for each d3m command at every level of
the command chain by using the `-h` flag. This is useful especially for the
getting a list of all the possible arguments for the `runtime` module.

```shell
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m -h
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m index -h
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m runtime -h
docker run --rm registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 python3 -m d3m runtime fit-score -h
```

One last point before we try running a pipeline. The docker container must be
able to access the dataset location and the pipeline location from the host
filesystem. We can do this by
[bind-mounting](https://docs.docker.com/storage/bind-mounts/) a host directory
to a container directory that contains both the `datasets` repo and the
`primitives` index repo (`git clone` them now if you haven't yet - you can find
links to them in the first section above). For example, if your directory
structure looks like this:

```
/home/foo/d3m
├── datasets
└── primitives
```

Then you'll want to bind-mount `/home/foo/d3m` to a directory in the container,
say `/mnt/d3m`. You can specify this mapping in the docker command itself:

```shell
docker run \
    --rm \
    -v /home/foo/d3m:/mnt/d3m \
    registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2019.11.10 \
    ls /mnt/d3m
```
