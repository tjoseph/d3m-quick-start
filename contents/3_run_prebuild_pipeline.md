## Running a pre-build pipeline

At this point, let's try running a pipeline. Again, we're going to run the
sample pipeline that comes with
`d3m.primitives.classification.logistic_regression.SKlearn`. There are two ways
to run a pipeline: by specifying all the necessary paths of the dataset, or by
specifying and using a pipeline run output file.

Remember to change the bind mount paths as appropriate for your system
(specified by `-v`).

### Specifying all the necessary paths of a dataset

You can use this if there is no existing pipeline run output yet for a pipeline,
or if you want to manually specify the dataset path (set the paths for `-r`,
`-i`, `-t`, `-a`, `-p` to your target dataset location).

```shell
docker run \
    --rm \
    -v /home/foo/d3m:/mnt/d3m \
    registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 \
    python3 -m d3m \
        runtime \
        fit-score \
            -r /mnt/d3m/datasets/seed_datasets_current/185_baseball/185_baseball_problem/problemDoc.json \
            -i /mnt/d3m/datasets/seed_datasets_current/185_baseball/TRAIN/dataset_TRAIN/datasetDoc.json \
            -t /mnt/d3m/datasets/seed_datasets_current/185_baseball/TEST/dataset_TEST/datasetDoc.json \
            -a /mnt/d3m/datasets/seed_datasets_current/185_baseball/SCORE/dataset_TEST/datasetDoc.json \
            -p /mnt/d3m/primitives/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipelines/862df0a2-2f87-450d-a6bd-24e9269a8ba6.json \
            -o /mnt/d3m/pipeline-outputs/predictions.csv \
            -O /mnt/d3m/pipeline-outputs/run.yml
```

The score is displayed after the pipeline run. The output predictions will be
stored on the path specified by `-o`, and information about the pipeline run is
stored in the path specified by `-O`.

Again, you can use the `-h` flag on `fit-score` to access the help string and
read about the different arguments, as described earlier.

### Using a pipeline run output

Instead of specifying all the specific dataset paths, you can also use an
existing pipeline run output to essentially "re-run" a previous run of the
pipeline.

We first have to decompress the gzipped pipeline run output that comes with the
sklearn primitive:

```shell
gzip -d /home/foo/d3m/primitives/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipeline_runs/pipeline_run.yml.gz
```

With that, we can now do this:

```shell
docker run \
    --rm \
    -v /home/foo/d3m:/mnt/d3m \
    registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 \
    python3 -m d3m \
        -p /mnt/d3m/primitives/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipelines \
        runtime \
            -d /mnt/d3m/datasets/seed_datasets_current/185_baseball \
        fit-score \
            -u /mnt/d3m/primitives/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipeline_runs/pipeline_run.yml \
            -o /mnt/d3m/pipeline-outputs/predictions.csv \
            -O /mnt/d3m/pipeline-outputs/run.yml
```

In this case, `-u` is the pipeline run output file that this pipeline will
re-run, and `-O` is the new pipeline run output file that will be generated.

Note that if you choose `fit-score` for the d3m runtime option, the pipeline
actually runs in two phases: fit, and produce. You can verify this by searching
for `phase` in the pipeline run output file.

Lastly, if you want to run multiple commands in the docker container, simply
chain your commands with `&&` and wrap them double quotes (`"`) for `bash -c`.
As an example:

```shell
docker run \
    --rm \
    -v /home/foo/d3m:/mnt/d3m \
    registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.1.9 \
    /bin/bash -c \
        "python3 -m d3m \
            -p /mnt/d3m/primitives/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipelines \
            runtime \
                -d /mnt/d3m/datasets/seed_datasets_current/185_baseball \
            fit-score \
                -u /mnt/d3m/primitives/v2020.1.9/JPL/d3m.primitives.classification.logistic_regression.SKlearn/2019.11.13/pipeline_runs/pipeline_run.yml \
                -o /mnt/d3m/pipeline-outputs/predictions.csv \
                -O /mnt/d3m/pipeline-outputs/run.yml &&
        head /mnt/d3m/pipeline-outputs/predictions.csv"
```
