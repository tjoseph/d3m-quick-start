# D3M-TA1 Quick Start Guide

This document aims to be a tutorial, or a quick-start guide for newcomers to
the **D3M-TA1** project. It is not meant to be a comprehensive guide to the
complete D3M, or even everything related to TA1.
The goal here, is for the user to get started with building TA1 primitives,
running unit tests, writing and running pipelines successfully.

This documentation is supported for current D3M version-`V2020.1.9`

## Important Links

First, here is a list of some important links that should help you with
reference and instructional material beyond this quick start guide. Be aware
also that the d3m core source code has extensive docstrings that you may find
helpful (the `d3m` repo can be accessed from the Public Code link below).

### Public
- Docs:                https://docs.datadrivendiscovery.org/
- Code:                https://gitlab.com/datadrivendiscovery
- Datasets:            https://datasets.datadrivendiscovery.org/d3m/datasets
- Docker images:       https://docs.datadrivendiscovery.org/docker.html
- TA1, TA2, TA3 repos: https://github.com/darpa-i2o/d3m-program-index


### Internal
- Wiki:     https://datadrivendiscovery.org/wiki/#all-updates
- Code:     https://gitlab.datadrivendiscovery.org/explore/groups
- Datasets: https://gitlab.datadrivendiscovery.org/d3m/datasets

## Contents

1. [Primitives and Pipelines](./contents/1_primitives_pipelines.md)
2. [Setting up D3M environment](./contents/2_Setting_D3M_env.md)
3. [Running a pre-build pipeline](./contents/3_run_prebuild_pipeline.md)
4. [TA1 Primitive Development - 1: Primitive](./contents/4_project_setup.md)
5. [TA1 Primitive Development - 2: Pipeline](./contents/4_project_setup_2.md)
6. [Helpful tips](./contents/5_helpful_tips.md)

### Contributors
<img src="images/ubc.png" width="63" height="86"> <img src="images/arrayfire.png" width="100" height="100">
